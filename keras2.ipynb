{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Keras 2 - recurrent networks\n",
    "\n",
    "Last week we looked at using feed-forward networks for regression and classification. This week we will look at using recurrent networks to predict timeseries data.\n",
    "\n",
    "Recurrent networks are used a lot for modelling language (text), which is a kind of time series. \n",
    "\n",
    "This tutorial assumes a very limited knowledge of deep learning (if you missed Keras 1 you should still be able to follow)\n",
    "\n",
    "A recurrent network with a single mid-layer looks like this:\n",
    "\n",
    "                          Input -> Layer -> Output\n",
    "                                /\\       |\n",
    "                                 |_______|\n",
    "\n",
    "\n",
    "The only difference compared to last week is the path looping back from the output to the input of the layer. It is this loop which acts as a form of \"memory\".\n",
    "\n",
    "### To clarify what is meant by the loop it is useful to look at the same network for different input:\n",
    "\n",
    "![diagram](data/rnn_diagram.png)\n",
    "\n",
    "\n",
    "#### Straight away we can see that this type of network assumes the data come in a certain order (like time series)\n",
    "\n",
    "The red arrows signify that the output of the layer gets passed on to that same layer and combined with a new input. \n",
    "\n",
    "##### Different RNNs have different ways of combining the red arrow with the input\n",
    "\n",
    "For example, Simple (vanilla) RNNs do the following:\n",
    "\n",
    "    * Multiply the output of the layer by some matrix of weights\n",
    "    * Multiply the input by another matrix of weights\n",
    "    * Add the two things above\n",
    "    * Apply a nonlinear function \n",
    "    \n",
    "Here we will learn about vanilla RNNs and the more popular LSTMs. \n",
    "\n",
    "Let's build our first RNN!\n",
    "\n",
    "\n",
    "---"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Part 1. Understanding RNNs"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from keras.models import Sequential\n",
    "from keras.layers import Dense, SimpleRNN, TimeDistributed\n",
    "\n",
    "rnn = Sequential()\n",
    "rnn.add( SimpleRNN(units=10, return_sequences=True,\n",
    "                   activation='linear',input_shape=(20,1)))\n",
    "\n",
    "rnn.add( TimeDistributed(Dense(units=1)))\n",
    "\n",
    "rnn.summary()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### We have created a Input->Layer->Output recurrent model.\n",
    "\n",
    "There are a few things that are different here to the simple feedforward network we had last time. So let us first create a feedforward network and contrast the two to understand the new bits.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ffn = Sequential()\n",
    "ffn.add( Dense(units=10,\n",
    "              activation='linear',input_shape=(1,)))\n",
    "\n",
    "ffn.add( Dense(units=1))\n",
    "\n",
    "ffn.summary()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### So what is different in our simple RNN?\n",
    "\n",
    "#### Number of parameters\n",
    "\n",
    "We saw in Keras 1 how to calculate the 31 parameters of this model (1x10 weights, 10 biases, and another 10x1 weights and 1 bias for the output)\n",
    "\n",
    "The extra 100 parameters in the RNN are the recurrent weight matrix. Since we have 10 units in the layer, the matrix is 10x10, which gives 131 total. Simple!\n",
    "\n",
    "\n",
    "#### input_shape\n",
    "\n",
    "This has an extra element (set here to =20). It is the number of sequential data points that are considered in doing either a forward prediction or in the optimization (gradient calculations). \n",
    "\n",
    "\n",
    "#### return_sequences\n",
    "\n",
    "Flag to say whether we want the output at each of the 20 time points or just at the last time point. This should be set to true when stacking multiple RNNs.\n",
    "\n",
    "#### TimeDistributed wrapper\n",
    "\n",
    "This applies the final layer to all the 20 different outputs. \n",
    "\n",
    "\n",
    "\n",
    "### Before I forget let's import numpy and pyplot - very useful :)\n",
    "\n",
    "## The picture below may help understand these last two things\n",
    "\n",
    "\n",
    "![return_sequences](data/return_sequences.png)\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import matplotlib.pyplot as plt\n",
    "%matplotlib inline \n",
    "import numpy as np\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### So these RNNs have a memory do they? \n",
    "\n",
    "To understand what is meant by memory, it is useful to look at our RNN's \"impule response\". \n",
    "\n",
    "Let's create an input that looks like a delta function at time=0 and look at the network outputs through time. \n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "delta = np.array([1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0])\n",
    "\n",
    "# Note: input must be 3D: batch_size X time_steps X features\n",
    "out=rnn.predict(delta.reshape((1,-1,1)))   \n",
    "\n",
    "# Note: output here also has 20 time steps because of TimeDistributed wrapper\n",
    "print(out.shape)\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Plot the output to a delta function:\n",
    "plt.plot(delta)\n",
    "plt.plot(out.flatten())\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### As you can see, despite the fact that the network only receives a single pulse at time=0, the ouput has 'reverberations', i.e. a trace of the pulse that persists throughout time.\n",
    "\n",
    "Let us contrast this with the Feedforward network:\n",
    "\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "\n",
    "out_rnn = rnn.predict(delta.reshape((1,-1,1)))\n",
    "out_ffn = ffn.predict(delta)\n",
    "\n",
    "plt.figure()\n",
    "plt.plot(delta)\n",
    "plt.plot(out_rnn[0,:,0])\n",
    "plt.plot(out_ffn)\n",
    "plt.legend(('Input','RNN','FFN'))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### So obviously the FFN has zero ouput for zero input (biases are all zeros by default) and because of the lack or recurrent connection there is no memory.\n",
    "\n",
    "### Now let us understand the output of the network a little better by trying to reproduce it with a For Loop:\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Reproducing the output of rnn.predict \n",
    "# with our own loop through time\n",
    "\n",
    "# Note: all biases are initialised to zero \n",
    "# so we only have matrix multiplications\n",
    "\n",
    "W1 = rnn.get_weights()[0]   # Input weights\n",
    "V  = rnn.get_weights()[1]   # Recurrent matrix\n",
    "W2 = rnn.get_weights()[-2]  # Output weights\n",
    "\n",
    "L      = np.zeros((1,10))      # 10 units\n",
    "my_out = np.zeros((20))        # 20 time points\n",
    "for i in range(20):\n",
    "    input = delta[i] \n",
    "    L = ((W1*input + L@V))     # A feedforward network would not have L@V here\n",
    "    my_out[i] = L@W2           # Output\n",
    "    \n",
    "\n",
    "# Compare ours to the network's pred\n",
    "plt.plot(out_rnn[0,:,0],marker='x')\n",
    "plt.plot(my_out)\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Superb.\n",
    "\n",
    "\n",
    "#### One question you may ask is why does the memory persist for so long, why does it not die out? -- Good question. \n",
    "\n",
    "The reason is that our network is linear (see the activation function we defined above), and by default in Keras, the RNN is initialised with matrix whose eigenvalues all have unit norm. This means the \"energy\" of the input does not get dissipated by repeatedly multiplying by the weight matrix defined V above.  \n",
    "\n",
    "Let's try with different activation functions (feel free to change the below to try tanh, sigmoid, relu, etc.). You can also try with different number of units.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "\n",
    "## BELOW: Copy of the code from the above cells\n",
    "## Run this with different activations, units, etc.\n",
    "\n",
    "# Redefined RNN with different recurrent activation\n",
    "\n",
    "units = 10\n",
    "\n",
    "rnn = Sequential()\n",
    "rnn.add( SimpleRNN(units=units, return_sequences=True,\n",
    "                   activation='relu',input_shape=(20,1)))\n",
    "\n",
    "rnn.add( TimeDistributed(Dense(units=1)))\n",
    "\n",
    "# Predict output\n",
    "out_rnn = rnn.predict(delta.reshape((1,-1,1)))\n",
    "\n",
    "\n",
    "# Replicate output with a For loop\n",
    "W1 = rnn.get_weights()[0]   \n",
    "V  = rnn.get_weights()[1]   \n",
    "W2 = rnn.get_weights()[-2]  \n",
    "\n",
    "L      = np.zeros((1,units))      \n",
    "my_out = np.zeros((20))        \n",
    "for i in range(20):\n",
    "    input = delta[i] \n",
    "    L = np.tanh((W1*input + L@V))     \n",
    "    my_out[i] = L@W2           \n",
    "    \n",
    "\n",
    "# Compare ours to the network's pred\n",
    "plt.figure()\n",
    "plt.plot(out_rnn[0,:,0],marker='x')\n",
    "plt.plot(my_out)\n",
    "plt.legend(('RNN','My_RNN'))\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### If you have played with the code above a little, you should have noticed that the ReLu activation is very good at dampening the input signal and thus shorten the memory (because it is quite easy for input to end up on the wrong end of a ReLu)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### An interesting remark about memory:\n",
    "\n",
    "Can we say what the memory of this RNN is? Let's plot the output to different scaled delta functions:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.plot(rnn.predict(delta.reshape((1,-1,1)))[0,:,0])\n",
    "plt.plot(rnn.predict(10*delta.reshape((1,-1,1)))[0,:,0])\n",
    "plt.plot(rnn.predict(20*delta.reshape((1,-1,1)))[0,:,0])\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "So the network remembers stronger things for longer, which seems obvious. But this is just the same as saying the memory depends on the weight matrices. \n",
    "\n",
    "In more interesting applications where the input is multidimensional (rather than 1D like here), each dimension can thus have its own memory, and therefore the memory depends on the input (not just the input magnitude, but also the input 'direction'). \n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Gated recurrent nets\n",
    "\n",
    "Gated recurrent nets are those where the input and the recurrent connection (the red arrow in our diagram at the top) are combined in a more complicated way that in the vanilla RNN. \n",
    "\n",
    "In the vanilla RNN, the input and recurrent output are just added to each other.\n",
    "\n",
    "In gated RNN, they can be combined in nonlinear ways, which makes the behaviour of the memory potentially more complicated.\n",
    "\n",
    "In Keras, it is easy to implement your own gated RNN. If you do so, make sure you give it a proper 3 or 4 letter acronym.\n",
    "\n",
    "## LSTMs\n",
    "\n",
    "LSTM stands for Long and Short term memory.\n",
    "\n",
    "It is a special case of the so called \"Gated\" recurrent nets.\n",
    "\n",
    "You can find decriptions of the way LSTMs do this combination on the interweb. Here we will instead just examine the memory behaviour of LSTMs.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from keras.layers import LSTM\n",
    "\n",
    "lstm = Sequential()\n",
    "lstm.add( LSTM(units=10, return_sequences=True,\n",
    "                   activation='relu',input_shape=(20,1)))\n",
    "\n",
    "lstm.add( TimeDistributed(Dense(units=1)))\n",
    "\n",
    "lstm.summary()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### We have only changed one word, and we have ended up with 491 parameters!\n",
    "\n",
    "* This is because the LSTM gating has multiple weight matrices for the multiple ways in which the input is combined with the recurrent output.\n",
    "\n",
    "* Since we are not aiming to described what LSTMs have under their bonnet, we are not going to try to calculate the number of parameters\n",
    "\n",
    "#### Let's look at the impulse response"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "plt.plot(lstm.predict(delta.reshape((1,-1,1)))[0,:,0])\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### If you play with the code above a little (running it for different network instantiations and for different activations, you should notice that:\n",
    "\n",
    "* The memory is always nice a smooth and dies out eventually\n",
    "* This makes them easier to train with backpropagation than vanilla RNNs\n",
    "* Sometimes you have memories that peak late. This is what is meant by short and long term: depending on the weights AND the input, you can have traces of the input that die out or produce an 'echo'\n",
    "---"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Part 2: Fitting RNNs to data\n",
    "\n",
    "So far we've only played with networks with random weights. In applications we fit those weights to data. And so the behaviour of the networks (e.g. memory) is data dependent.\n",
    "\n",
    "We will begin with a simple example of fitting a sine function, and then we will move on to a real 1D signal (stock prices).\n",
    "\n",
    "#### Note: although the more interesting applications are perhaps language modelling, the maths and the understanding is the same as for 1D signal, but there is a lot more overhead in terms of e.g. turning the text into numbers, which gets in the way of understanding the networks. So we won't be doing text modelling here.\n",
    "\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# creating toy data set using the sine function \n",
    "# the objective is to predict sin(t+dt) from the history of sin(t) \n",
    "\n",
    "import numpy as np\n",
    "\n",
    "n_samples = 200\n",
    "\n",
    "t = np.linspace(0,40,n_samples)\n",
    "dt = t[1]-t[0]   # get dt \n",
    "x = np.sin(t)    # Input \n",
    "y = np.sin(t+dt) # Target \n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# WE NEED A HELPER FUNCTION WHICH 'WINDOWS'\n",
    "# THE DATA so we can feed it to the RNNs\n",
    "\n",
    "def create_window(x,time_steps=1):\n",
    "    y = []\n",
    "    for k in range(0,len(x)-time_steps):\n",
    "        y.append(x[k:k+time_steps])\n",
    "    return np.asarray(y)\n",
    "\n",
    "# Apply to the input\n",
    "xx=create_window(x,20)\n",
    "\n",
    "# Since time_steps=20, we want the target to be the 21st time step onwards\n",
    "\n",
    "yy = x[20:].reshape(-1,1)\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Quick note\n",
    "* In the Keras 1 prac, we created the regression model t->x \n",
    "* This is not something that RNNs are going to be useful at, given that we are interested in predicting the future. Remember neural nets cannot extrapolate beyond the input they have seen\n",
    "* So the trick is to have the 'history of the data' as an input. \n",
    "* And so we are fitting the model x(t)->x(t+dt)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Before fitting the model to the data we need to 'compile' it\n",
    "\n",
    "This is where we can choose the loss function (to be minimized) and the optimization algorithm. \n",
    "\n",
    "I picked stochastic gradient descent (sgd) and mean squared error (mse) for the loss. \n",
    "\n",
    "We fit with .fit() [just like in scikitlearn if you remember]  - the number of epochs is the number of times the entire dataset is utilised in the gradient descent."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# Let's recreate a RNN, but this time only output the last time point\n",
    "# just to see how it is done :)\n",
    "\n",
    "rnn = Sequential()\n",
    "rnn.add( SimpleRNN(units=10, return_sequences=False,\n",
    "                   activation='linear',input_shape=(20,1)))\n",
    "\n",
    "rnn.add( Dense(units=1))\n",
    "\n",
    "# Compile and fit the RNN\n",
    "rnn.compile(optimizer='sgd', loss='mse')\n",
    "rnn.fit(xx.reshape(-1,20,1),\n",
    "        yy,\n",
    "        epochs=100, batch_size=100)\n",
    "\n",
    "\n",
    "# For comparison, compile and fit the FFN\n",
    "ffn.compile(optimizer='sgd', loss='mse')\n",
    "ffn.fit(x,\n",
    "        y,\n",
    "        epochs=100, batch_size=100)\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Let's look at the fit"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# ## FFN prediction\n",
    "plt.figure()\n",
    "plt.scatter(x,y)\n",
    "out_ffn=ffn.predict(x)\n",
    "plt.scatter(x,out_ffn,marker='x')\n",
    "\n",
    "## RNN prediction\n",
    "plt.figure()\n",
    "plt.scatter(xx[:,-1],yy)\n",
    "out_rnn=rnn.predict(xx[:,:,None])\n",
    "plt.scatter(xx[:,-1],out_rnn,marker='x')\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## It is clear that the FFN is hopeless. Given x, it is unable to decide whether to produce the higher or lower y. \n",
    "## The RNN knows the past, and therefore can decide whether it is ascending or descending the sine function. \n",
    "## Note: this can be achieved with only 2 time steps, no need for 20 (try it at home) \n",
    "\n",
    "---\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Part 3. Predicting the future\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### We have been using the input x to predict the output y. But we have not used proper train/test data, so it is cheating. Let's learn how to predict the future data.\n",
    "### To do this, we need to use the ouput of the network as new input.\n",
    "\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "\n",
    "# Get one of the 'batches' as a starting point\n",
    "x0          = xx[0,:].copy()\n",
    "\n",
    "# Initialise\n",
    "current_x   = x0\n",
    "time_series = x0\n",
    "\n",
    "# Loop over time points\n",
    "for i in range(200):\n",
    "    # make a prediction for the next time point\n",
    "    pred = rnn.predict(current_x[None,:,None])\n",
    "    # append\n",
    "    time_series = np.append(time_series,pred[0])\n",
    "    # shift \n",
    "    current_x[:-1] = current_x[1:]\n",
    "    current_x[-1] = pred[0]\n",
    "\n",
    "plt.plot(time_series)\n",
    "\n",
    "# DO THE SAME FOR FFN\n",
    "x0 = xx[0,:].copy()\n",
    "current_x   = x0\n",
    "time_series = x0\n",
    "for i in range(200):\n",
    "    # make a prediction for the next time point\n",
    "    pred = ffn.predict(current_x)\n",
    "    # append\n",
    "    time_series = np.append(time_series,pred[0])\n",
    "    # shift \n",
    "    current_x[:-1] = current_x[1:]\n",
    "    current_x[-1] = pred[0]\n",
    "\n",
    "\n",
    "plt.plot(time_series)\n",
    "\n",
    "plt.legend(('RNN','FNN'))\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Interesting... \n",
    "\n",
    "The RNN is very good at predicting the future of sine. \n",
    "The FFN just keeps giving the same first 20 data points repeatedly..\n",
    "\n",
    "\n",
    "## What happens when I fit nonlinear RNNs to this sine thingy?\n",
    "\n",
    "Our RNN is linear. If you recall, linear RNNs can have long lasting memory. What happens when I use a nonlinear RNN?\n",
    "\n",
    "\n",
    "\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Go back up, change the nonlinear activation of the rnn, \n",
    "# compile and fit to the sine, and come back here to run the code above\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## OMG this is so cool.\n",
    "\n",
    "* The RNN keeps going until instabilities occur.\n",
    "* The FNN just keep repeating the input sequence (of course it does)\n",
    "* BUT: nonlinearities can cause quite dramatic memory effects. \n",
    "* Whether we want long or short term memories depends on the applications.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "## But hold on - you guys have just fitted a sine wave again. We KNOW how sine works. Can't we PLEASE do something more interesting?\n",
    "\n",
    "## Fine. Let's predict predict the stock market!\n",
    "\n",
    "-----"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Part 4. How to make money with RNNs\n",
    "\n",
    "We finish this tutorial with a real data example where we give you a step-by-step recipe for predicting stock market prices in order to make some cash.\n",
    "\n",
    "\n",
    "### Step 1 : Get some stock trends"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import pandas as pd\n",
    "df = pd.read_csv('data/stocks.csv')\n",
    "#setting index as date\n",
    "df['Date'] = pd.to_datetime(df.Date,format='%Y-%m-%d')\n",
    "df.index = df['Date']\n",
    "\n",
    "df.head(10)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "Look at us, we are using Pandas data frames! Isn't it nice when everything comes together nicely?\n",
    "\n",
    "\n",
    "#### The value of interest for us here is the \"close\" price. Let's see what it looks like over time:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "data = np.flip(np.array(df['Close']))  # to get time running in the correct direction\n",
    "data = data[:,None] # Turn into 1D vector\n",
    "plt.plot(data)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Step 2: Split into Test and Train batches\n",
    "\n",
    "Let's train on the first 1000 datapoints, and test on the next 500. The last bit of the data looks a bit too tricky for a one hour tutorial.\n",
    "\n",
    "Note: we start by scaling the data because it is always a good idea to scale the data"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "\n",
    "# Scaling the data\n",
    "# Should actually only be fit to training data and \n",
    "# applied to test data - let's ignore this ;-)\n",
    "from sklearn.preprocessing import MinMaxScaler\n",
    "scaler = MinMaxScaler(feature_range=(0, 1))\n",
    "data = scaler.fit_transform(data)\n",
    "\n",
    "data_train = data[:1000]\n",
    "data_test  = data[1000:1500]\n",
    "\n",
    "# Time bins so we can plot \n",
    "# training and test next to each other\n",
    "t_train = np.arange(1000)\n",
    "t_test  = np.arange(500)+1000\n",
    "\n",
    "# Plot the data\n",
    "plt.plot(t_train, data_train, label='train')\n",
    "plt.plot(t_test, data_test, label='test')\n",
    "plt.legend(loc='best')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Step 3: Prepare the data\n",
    "\n",
    "Here we will assume the network will utilise 7 time points to predict the 8th"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "train_windowed = create_window(data_train,7)\n",
    "target        = data_train[7:]\n",
    "print(train_windowed.shape)\n",
    "print(target.shape)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Step 4: Create RNN (assuming 7-day history)\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create network with LSTM\n",
    "net = Sequential()\n",
    "net.add( LSTM(units=200, name='layer1', input_shape=(7,1)))\n",
    "net.add( Dense(units=1,name='output'))\n",
    "net.summary()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# I guess we are ready to optimise\n",
    "\n",
    "net.compile(optimizer='adam',loss='mse')\n",
    "net.fit(train_windowed,target,epochs=10,batch_size=100)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Let us first visualise the predictions agains the data in the training data\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "\n",
    "pred = net.predict(train_windowed)\n",
    "\n",
    "plt.figure()\n",
    "plt.plot(target)\n",
    "plt.plot(pred)\n",
    "\n",
    "\n",
    "# Zoom in on a bit of the plot\n",
    "plt.figure()\n",
    "plt.plot(target)\n",
    "plt.plot(pred)\n",
    "plt.xlim(800,850)\n",
    "plt.ylim(0.2,0.4)\n",
    "plt.title('Test Dataset Cropped')\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## What is it doing?? \n",
    "\n",
    "It looks like it is shifting the whole thing one day into the future doesn't it?\n",
    "\n",
    "Well, to find out what it is really doing, we need to predict the test data"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "test_windowed = create_window(data_test,7)\n",
    "target        = data_test[7:]\n",
    "pred = net.predict(test_windowed)\n",
    "\n",
    "plt.figure()\n",
    "plt.plot(target)\n",
    "plt.plot(pred)\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## It looks good doesn't it?\n",
    "\n",
    "### But are we really going to make millions out of it? \n",
    "\n",
    "Think about it, all we are doing is predict the next data point using the 7 previous days. But our fits look like they are just shifting... What we really ought to do is predict the future using just the past and only the past. \n",
    "\n",
    "So we need to run the model on the training data (with the sliding window), but then we need to stop giving it real data and start giving it its own predictions when it comes to the test data!\n",
    "\n",
    "Let's do that. That's proper predictive modelling.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "\n",
    "# Run on all the training data\n",
    "# (this gets the ''memory' going)\n",
    "pred_on_train = net.predict(train_windowed,batch_size=1000)\n",
    "\n",
    "\n",
    "# Get the last 7 days (this could be increased if you want better predictions)\n",
    "x0          = pred_on_train[-7:].copy().flatten()\n",
    "current_x   = x0\n",
    "time_series = x0\n",
    "\n",
    "# predict n_timepoints into the future\n",
    "n_timepoints = 100\n",
    "for i in range(n_timepoints):\n",
    "    # make a prediction for the next time point\n",
    "    pred = net.predict(current_x[None,:,None])\n",
    "    # append\n",
    "    time_series = np.append(time_series,pred[0])\n",
    "    # shift \n",
    "    current_x[:-1] = current_x[1:]\n",
    "    current_x[-1] = pred[0]\n",
    "\n",
    "plt.plot(t_train,data_train)\n",
    "plt.plot(t_test[:n_timepoints],data_test[:n_timepoints])\n",
    "\n",
    "plt.plot(t_test[:n_timepoints],time_series[7:])\n",
    "\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Boahahaha! Call that a prediction? :)\n",
    "\n",
    "Well let's see if it gets better with using \n",
    "\n",
    "* Longer time windows\n",
    "* Deeper networks\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "\n",
    "\n",
    "# Create network with LSTM\n",
    "nsteps = 30   # one month\n",
    "net = Sequential()\n",
    "net.add( LSTM(units=200, name='layer1', input_shape=(nsteps,1), return_sequences=True))\n",
    "net.add( LSTM(units=200, name='layer2'))\n",
    "net.add( Dense(units=1,name='output'))\n",
    "net.summary()\n",
    "\n",
    "# Prepare data\n",
    "train_windowed = create_window(data_train,nsteps)\n",
    "target        = data_train[nsteps:]\n",
    "\n",
    "# Fit\n",
    "net.compile(optimizer='adam',loss='mse')\n",
    "net.fit(train_windowed,target,epochs=10,batch_size=64)\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Start with last time window\n",
    "x0          = train_windowed[-1,:].copy().flatten()\n",
    "current_x   = x0\n",
    "time_series = x0\n",
    "\n",
    "n_timepoints = 500\n",
    "for i in range(n_timepoints):\n",
    "    # make a prediction for the next time point\n",
    "    pred = net.predict(current_x[None,:,None])\n",
    "    # append\n",
    "    time_series = np.append(time_series,pred[0])\n",
    "    # shift \n",
    "    current_x[:-1] = current_x[1:]\n",
    "    current_x[-1] = pred[0]\n",
    "\n",
    "plt.plot(t_train,data_train)\n",
    "plt.plot(t_test[:n_timepoints],data_test[:n_timepoints])\n",
    "\n",
    "plt.plot(t_test[:n_timepoints],time_series[nsteps:])\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Not so good.... \n",
    "\n",
    "# Step 5. Learn your lesson.\n",
    "\n",
    "* We are clearly badly overfitting. To overcome this, we need the bags of tricks that modern deep learning uses:\n",
    "        - Shed loads of data (we only had 1000 time points, we need millions)\n",
    "        - Use regularisation (Dropout etc.)\n",
    "        \n",
    "* Importantly, and something that is often forgotten but is the Machine Learning 101:\n",
    "    - RNNs are pattern learners. \n",
    "    - They can reliably find patterns (sometimes very complex, and nonlinear) in a dataset \n",
    "    - BUT ONLY IF THOSE PATTERNS ARE THERE!!!\n",
    "    \n",
    "    - In other words, our LSTM network would be able to learn any of the internal drivers of the stock price, but the truth is that MOST of the drivers will actually be external. But don't loose hope!\n",
    "\n",
    "There are 2 options now. Either, you need to find those external drivers and include them as features in your network, OR find an application which is entirely described by its internal structure. A good example of the latter is learning a language from texts. All of the rules of how to form syntactically correct sentences are in a book, and a paragraph is just learning the rules for adding sentences etc. etc. Hopefully now though you have some understanding of RNNs (or at least know that they exist) and maybe you'll be able to spot a way of applying them to your own work."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# The end."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.8"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
