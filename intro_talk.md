# Introduction to data analysis in Python


This talk is an attempt to give a whirlwind overview of the Python programming
language, and its main uses in data analysis. Most of the content presented
here has been taken from material originally written for the 2018 WIN PyTreat
(**Py**thon re**Treat**), by Michiel Cottaar, Mark Jenkinson, and Paul
McCarthy.

It is assumed that you have experience with another programming language
(e.g. MATLAB).

> If you want to learn more about Python, you can access all of the material
> from the PyTreat
> [here](https://git.fmrib.ox.ac.uk/fsl/pytreat-2018-practicals).

This talk is presented as an interactive [Jupyter
Notebook](https://jupyter.org/) - you can run all of the code on your own
machine - click on a code block, and press **SHIFT+ENTER**. You can also "run"
the text sections, so you can just move down the document by pressing
**SHIFT+ENTER**.

It is also possible to _change_ the contents of each code block (these pages
are completely interactive) so do experiment with the code you see and try
some variations!

You can get help on any Python object, function, or method by putting a `?`
before or after the thing you want help on:

```
a = 'hello!'
?a.upper
```

And you can explore the available methods on a Python object by using the
**TAB** key:

```
# Put the cursor after the dot, and press the TAB key...
a.
```


<a class="anchor" id="python-in-a-nutshell"></a>
# Python in a nutshell


## Pros


* _Flexible_ Feel free to use functions, classes, objects, modules and
  packages. Or don't - it's up to you!

* _Fast_ If you do things right (in other words, if you use `numpy`)

* _Dynamically typed_ No need to declare your variables, or specify their
  types.

* _Intuitive syntax_ How do I run some code for each of the elements in my
  list?

```
a = [1, 2, 3, 4, 5]

for elem in a:
    print(elem)
```


## Cons

* _Dynamically typed_ Easier to make mistakes, harder to catch them

* _No compiler_ See above

* _Slow_ if you don't do things the right way

* _Python 2 is not the same as Python 3_ But there's an easy solution: Forget
  that Python 2 exists.

* _Hard to manage different versions of python_ But we have a solution for
  you: `fslpython`.


Python is a widely used language, so you can get lots of help through google
and [stackoverflow](https://stackoverflow.com). But make sure that the
information you find is for **Python 3**, and **not** for **Python 2**!
Python 2 is obsolete, but is still used by many organisations, so you will
inevitably come across many Python 2 resources.

The differences between Python 2 and 3 are small, but important. The most
visible difference is in the `print` function: in Python 3, we write
`print('hello!')`, but in Python 2, we would write `print 'hello!'`.


---
<a class="anchor" id="variables-and-basic-types"></a>
# Variables and basic types


There are many different types of values in Python. Python *variables* do not
have a type though - a variable can refer to values of any type, and a
variable can be updated to refer to different values (of different
types). This is just like how things work in MATLAB.


## Integer and floating point scalars

```
a = 7
b = 1 / 3
c = a + b
print('a:    ', a)
print('b:    ', b)
print('c:    ', c)
print('b:     {:0.4f}'.format(b))
print('a + b:', a + b)
```


## Strings

```
a = 'Hello'
b = "Kitty"
c = '''
Magic
multi-line
strings!
'''

print(a, b)
print(a + b)
print('{}, {}!'.format(a, b))
print(c)
```


String objects have a number of useful methods:

```
s = 'This is a Test String'
print(s.upper())
print(s.lower())
```


Another useful method is:

```
s = 'This is a Test String'
s2 = s.replace('Test', 'Better')
print(s2)
```


Two common and convenient string methods are `strip()` and `split()`.  The
first will remove any whitespace at the beginning and end of a string:

```
s2 = '   A very    spacy   string       '
print('*' + s2 + '*')
print('*' + s2.strip() + '*')
```


With `split()` we can tokenize a string (to turn it into a list of strings)
like this:

```
print(s.split())
print(s2.split())
```


We can also use the `join` method to re-construct a new string. Imagine that
we need to reformat some data from being comma-separated to being
space-separated:


```
data = ' 1,2,3,4,5,6,7  '
```


`strip`, `split` and `join` makes this job trivial:


```
print('Original:               {}'.format(data))
print('Strip, split, and join: {}'.format(' '.join(data.strip().split(','))))
```


## Lists and tuples

Both tuples and lists are built-in Python types and are like cell-arrays in
MATLAB. For numerical vectors and arrays it is much better to use _numpy_
arrays, which are covered later.

Tuples are defined using round brackets and lists are defined using square
brackets. For example:

```
t = (3, 7.6, 'str')
l = [1, 'mj', -5.4]
print(t)
print(l)

t2 = (t, l)
l2 = [t, l]
print('t2 is: ', t2)
print('l3 is: ', l2)
```


The key difference between lists and tuples is that tuples are *immutable*
(once created, they cannot be changed), whereas lists are *mutable*:

```
a = [10, 20, 30]
a = a + [70]
a += [80]
print(a)
```


Square brackets are used to index tuples, lists, strings, dictionaries, etc.
For example:

```
d = [10, 20, 30]
print(d[1])
```

> **MATLAB pitfall:** Python uses zero-based indexing, unlike MATLAB, where
> indices start from 1.

```
a = [10, 20, 30, 40, 50, 60]
print(a[0])
print(a[2])
```


A range of values for the indices can be specified to extract values from a
list or tuple using the `:` character.  For example:

```
print(a[0:3])
```


> **MATLAB pitfall:** Note that Python's slicing syntax is different from
> MATLAB in that the second number is _exclusive_, i.e. `a[0:3]` gives us the
> elements of `a` at positions `0`, `1` and `2` , but *not* at position `3`.


When slicing a list or tuple, you can leave the start and end values out -
when you do this, Python will assume that you want to start slicing from the
beginning or the end of the list.  For example:

```
print(a[:3])
print(a[1:])
print(a[:])
print(a[:-1])
```


You can also change the step size, which is specified by the third value (not
the second one, as in MATLAB).  For example:

```
print(a[0:4:2])
print(a[::2])
print(a[::-1])
```


Some methods are available on `list` objects for adding and removing items:

```
print(d)
d.append(40)
print(d)
d.extend([50, 60])
print(d)
d = d + [70, 80]
print(d)
d.remove(20)
print(d)
d.pop(0)
print(d)
```

What will `d.append([50,60])` do, and how is it different from
`d.extend([50,60])`?

```
d.append([50, 60])
print(d)
```


## Dictionaries

Dictionaries (or *dicts*) can be used to store key-value pairs. Almost
anything can used as a key, and anything can be stored as a value; it is
common to use strings as keys:

```
e = {'a' : 10, 'b': 20}
print(len(e))
print(e.keys())
print(e.values())
print(e['a'])
```


Like lists (and unlike tuples), dicts are mutable, and have a number of
methods for manipulating them:

```
e['c'] = 30
e.pop('a')
e.update({'a' : 100, 'd' : 400})
print(e)
e.clear()
print(e)
```


<a class="anchor" id="a-note-on-mutability"></a>
## A note on mutablility


Python variables can refer to values which are either mutable, or
immutable. Examples of immutable values are strings, tuples, and integer and
floating point scalars. Examples of mutable values are lists, dicts, and most
user-defined types.


When you pass an immutable value around (e.g. into a function, or to another
variable), it works the same as if you were to copy the value and pass in the
copy - the original value is not changed:

```
a = 'abcde'
b = a
b = b.upper()
print('a:', a)
print('b:', b)
```

In contrast, when you pass a mutable value around, you are passing a
*reference* to that value - there is only ever one value in existence, but
multiple variables refer to it. You can manipulate the value through any of
the variables that refer to it:

```
a = [1, 2, 3, 4, 5]
b = a

a[3] = 999
b.append(6)

print('a', a)
print('b', b)
```


---
<a class="anchor" id="flow-control"></a>
# Flow control

Python also has a boolean type which can be either `True` or `False`. Most
Python types can be implicitly converted into booleans when used in a
conditional expression.

Relevant boolean and comparison operators include: `not`, `and`, `or`, `==`
and `!=`

For example:
```
a = True
b = False
print('Not a is:', not a)
print('a or b is:', a or b)
print('a and b is:', a and b)
print('Not 1 is:', not 1)
print('Not 0 is:', not 0)
print('Not {} is:', not {})
print('{}==0 is:', {}==0)
```

There is also the `in` test for strings, lists, etc:

```
print('the' in 'a number of words')
print('of' in 'a number of words')
print(3 in [1, 2, 3, 4])
```

We can use boolean values in `if`-`else` conditional expressions:

```
a = [1, 2, 3, 4]
val = 3
if val in a:
    print('Found {}!'.format(val))
else:
    print('{} not found :('.format(val))
```

Note that the indentation in the `if`-`else` statement is **crucial**.
**All** python control blocks are delineated purely by indentation. We
recommend using **four spaces** and no tabs, as this is a standard practice
and will help a lot when collaborating with others.

You can use the `for` statement to loop over elements in a list:

```
d = [10, 20, 30]
for x in d:
    print(x)
```


You can also loop over the key-value pairs in a dict:

```
a = {'a' : 10, 'b' : 20, 'c' : 30}
print('a.items()')
for key, val in a.items():
    print(key, val)
print('a.keys()')
for key in a.keys():
    print(key, a[key])
print('a.values()')
for val in a.values():
    print(val)
```


> Note that there is no guarantee of ordering in the python `dict` type.  If
> you need ordering, you can use the
> [`collections.OrderedDict`](https://docs.python.org/3/library/collections.html#collections.OrderedDict)
> type instead.


There are some handy built-in functions that you can use with `for` loops:

```
d = [10, 20, 30]
print('Using the range function')
for i in range(len(d)):
    print('element at position {}: {}'.format(i, d[i]))

print('Using the enumerate function')
for i, elem in enumerate(d):
    print('element at position {}: {}'.format(i, elem))
```


<a class="anchor" id=" list-comprehensions"></a>
## List comprehensions


Python has a really neat way to create lists (and dicts), called
*comprehensions*. Let's say we have some strings, and we want to count the
number of characters in each of them:

```
strings = ['hello', 'howdy', 'hi', 'hey']
nchars = [len(s) for s in strings]
for s, c in zip(strings, nchars):
    print('{}: {}'.format(s, c))
```

> The `zip` function "zips" two or more sequences, so you can loop over them
> together.

Or we could store the character counts in a dict:


```
nchars = { s : len(s) for s in strings }

for s, c in nchars.items():
    print('{}: {}'.format(s, c))
```


<a class="anchor" id=" reading-and-writing-text-files"></a>
# Reading and writing text files

The syntax to open a file in python is
`with open(<filename>, <mode>) as   <file_object>: <block of code>`, where

* `filename` is a string with the name of the file
* `mode` is one of 'r' (for read-only access), 'w' (for writing a file, this
  wipes out any existing content), 'a' (for appending to an existing file).
* `file_object` is a variable name which will be used within the `block of
  code` to access the opened file.


For example the following will read all the text in `data/file.txt` and print
it:

```
with open('data/file.txt', 'r') as f:
    print(f.read())
```


A very similar syntax is used to write files:

```
with open('new_file.txt', 'w') as f:
    f.write('This is my first line\n')
    f.writelines(['Second line\n', 'and the third\n'])
```

<a class="anchor" id="example-processing-lesion-counts"></a>
## Example - Processing lesion counts


Imagine that we have written an amazing algorithm in Python which
automatically counts the number of lesions in an individual's structural MRI
image.

```
subject_ids   = ['01', '07', '21', '32']
lesion_counts = [  4,    9,   13,    2]
```


We may wish to process this data in another application (e.g. Excel or SPSS).
Let's save the results out to a CSV (comma-separated value) file:

```
with open('lesion_counts.csv', 'w') as f:
    f.write('Subject ID, Lesion count\n')
    for subj_id, count in zip(subject_ids, lesion_counts):
        f.write('{}, {}\n'.format(subj_id, count))
```


We can now load the `lesion_counts.csv` file into our analysis software of
choice. Or we could load it back into another Python session, and store
the data in a dict:


```
lesion_counts = {}

with open('lesion_counts.csv', 'r') as f:
    # skip the header
    f.readline()
    for line in f.readlines():
        subj_id, count = line.split(',')
        lesion_counts[subj_id] = int(count)

print('Loaded lesion counts:')
for subj, count in lesion_counts.items():
    print('{}: {}'.format(subj, count))
```



---
<a class="anchor" id="working-with-numpy"></a>
# Working with numpy


This section introduces you to [`numpy`](http://www.numpy.org/), Python's
numerical computing library. Numpy adds a new data type to the Python
language - the `array` (more specifically, the `ndarray`). A Numpy `array`
is a N-dimensional array of homogeneously-typed numerical data.

Pretty much every scientific computing library in Python is built on top of
Numpy - whenever you want to access some data, you will be accessing it in the
form of a Numpy array. So it is worth getting to know the basics.


<a class="anchor" id="the-python-list-versus-the-numpy-array"></a>
## The Python list versus the Numpy array


You have already been introduced to the Python `list`, which you can easily
use to store a handful of numbers (or anything else):


```
data = [10, 8, 12, 14, 7, 6, 11]
```


You could also emulate a 2D or ND matrix by using lists of lists, for example:


```
xyz_coords = [[-11.4,   1.0,  22.6],
              [ 22.7, -32.8,  19.1],
              [ 62.8, -18.2, -34.5]]
```


For simple tasks, you could stick with processing your data using python
lists, and the built-in
[`math`](https://docs.python.org/3.5/library/math.html) library. And this
might be tempting, because it does look quite a lot like what you might type
into Matlab.


But __BEWARE!__ A Python list is a terrible data structure for scientific
computing!


This is a major source of confusion for people who are learning Python, and
are trying to write efficient code. It is _crucial_ to be able to distinguish
between a Python list and a Numpy array.


___Python list == Matlab cell array:___ A list in Python is akin to a cell
array in Matlab - they can store anything, but are extremely inefficient, and
unwieldy when you have more than a couple of dimensions.


___Numpy array == Matlab matrix:___ These are in contrast to the Numpy array
and Matlab matrix, which are both thin wrappers around a contiguous chunk of
memory, and which provide blazing-fast performance (because behind the scenes
in both Numpy and Matlab, it's C, C++ and FORTRAN all the way down).


So you should strongly consider turning those lists into Numpy arrays:


```
import numpy as np

data = np.array([10, 8, 12, 14, 7, 6, 11])

xyz_coords = np.array([[-11.4,   1.0,  22.6],
                       [ 22.7, -32.8,  19.1],
                       [ 62.8, -18.2, -34.5]])
print('data:', data)
print('xyz_coords:', xyz_coords)
```


> Numpy is not a "built-in" library, so we have to import it. The statement
> `import numpy as np` tells Python to *Import the `numpy` library, and make
> it available as a variable called `np`.*


<a class="anchor" id="creating-arrays"></a>
## Creating arrays


Numpy has quite a few functions which behave similarly to their equivalents in
Matlab:


```
print('np.zeros gives us zeros:                       ', np.zeros(5))
print('np.ones gives us ones:                         ', np.ones(5))
print('np.arange gives us a range:                    ', np.arange(5))
print('np.linspace gives us N linearly spaced numbers:', np.linspace(0, 1, 5))
print('np.random.random gives us random numbers [0-1]:', np.random.random(5))
print('np.random.randint gives us random integers:    ', np.random.randint(1, 10, 5))
print('np.eye gives us an identity matrix:')
print(np.eye(4))
print('np.diag gives us a diagonal matrix:')
print(np.diag([1, 2, 3, 4]))
```


The `zeros` and `ones` functions can also be used to generate N-dimensional
arrays:


```
z = np.zeros((3, 4))
o = np.ones((2, 10))
print(z)
print(o)
```


> Note that, in a 2D Numpy array, the first axis corresponds to rows, and the
> second to columns - just like in Matlab.


<a class="anchor" id="generating-random-numbers"></a>
## Generating random numbers


Numpy's
[`numpy.random`](https://docs.scipy.org/doc/numpy/reference/routines.random.html)
module is where you should go if you want to introduce a little randomness
into your code.  You have already seen a couple of functions for generating
uniformly distributed real or integer data:


```
import numpy.random as npr

print('Random floats between 0.0 (inclusive) and 1.0 (exclusive):')
print(npr.random((3, 3)))

print('Random integers in a specified range:')
print(npr.randint(1, 100, (3, 3)))
```


You can also draw random data from other distributions - here are just a few
examples:


```
print('Gaussian (mean: 0, stddev: 1):')
print(npr.normal(0, 1, (3, 3)))

print('Gamma (shape: 1, scale: 1):')
print(npr.gamma(1, 1, (3, 3)))

print('Chi-square (dof: 10):')
print(npr.chisquare(10, (3, 3)))
```


Imagine we have some data, and we want to add a small amount of random noise
to it:

```
data = np.linspace(0, 5, 10)
# add uniform noise in the range -1, 1
noise = (npr.random(10) - 0.5) * 2

noisy_data = data + noise

print('original data: {}'.format(data))
print('noise:         {}'.format(noise))
print('noisy data:    {}'.format(noisy_data))
```

> **MATLAB pitfall:** Arithmetic operations on arrays in Numpy work on an
> *elementwise* basis. In particular, if you multiply two arrays together,
> you will get the elementwise product. You **won't** get the dot product,
> like you would in MATLAB. You can, however, use the `@` operator to perform
> matrix multiplication on numpy arrays.


The `numpy.random` module also has a couple of other handy functions for
random sampling of existing data:


```
data = np.arange(5)

print('data:               ', data)
print('two random values:  ', npr.choice(data, 2))
print('random permutation: ', npr.permutation(data))

# The numpy.random.shuffle function
# will shuffle an array *in-place*.
npr.shuffle(data)
print('randomly shuffled: ', data)
```

<a class="anchor" id="example-reading-arrays-from-text-files"></a>
## Example: reading arrays from text files


The `numpy.loadtxt` function is capable of loading numerical data from
plain-text files. By default it expects space-separated data:


```
data = np.loadtxt('data/space_separated.txt')
print('data in data/space_separated.txt:')
print(data)
```


But you can also specify the delimiter to expect<sup>1</sup>:


```
data = np.loadtxt('data/comma_separated.txt', delimiter=',')
print('data in data/comma_separated.txt:')
print(data)
```


> <sup>1</sup> And many other things such as file headers, footers, comments,
> and newline characters - see the
> [docs](https://docs.scipy.org/doc/numpy/reference/generated/numpy.loadtxt.html)
> for more information.


Of course you can also save data out to a text file just as easily, with
[`numpy.savetxt`](https://docs.scipy.org/doc/numpy/reference/generated/numpy.savetxt.html):


```
data = np.random.randint(1, 10, (10, 10))
np.savetxt('mydata.txt', data, delimiter=',', fmt='%i')
```

Jupyter notebooks have a special feature - if you start a line with a `!`
character, you can run a `bash` command. Let's look at the file we just
generated:


```
!cat mydata.txt
```

> The `!` feature won't work in regular Python scripts.


Here's how we can load a 2D array fom a file, and calculate the mean of each
column:

```
data     = np.loadtxt('data/2d_array.txt', comments='%')
colmeans = data.mean(axis=0)

print('Column means')
print('\n'.join(['{}: {:0.2f}'.format(i, m) for i, m in enumerate(colmeans)]))
```


<a class="anchor" id="example-concatenate-affine-transforms"></a>
## Example: concatenate affine transforms


> **30 second introduction to registration**
>
> MRI images of different modalities (e.g. structural, functional), and from
> different subjects, are usually not aligned to each other. So in order to
> analyse data from different modalities/subjects, we need to *register* the
> images together, so we can move data between the different spaces.
>
>
> ![registration](prep/reg.png)
>
>
> A standard form of registration is *linear* registration, where one of the
> images is rotated, shifted/translated, and scaled so that it is aligned to the
> next. These rotations, translations and scalings can be encoded as an *affine*
> transformation matrix. After we have calculated a transformation between each
> pair of spaces, we can combine the individual transfomations to move between
> any space.


Imagine that we have some fMRI data for two subjects, and we have defined
some ROI coordinates in the functional image for the first subject:


```
# ROI coordinates in the functional
# space for subject 1
subj1rois = np.array([[ 0,   0,  0],
                      [-5, -20, 10],
                      [20,  25, 60]], dtype=np.float32)
```

Now we want to transform those coordinates from the functional space of the
first subject into the functional space of the second: If we have linearly
registered each subject to a standard space, we can combine the individual
affine transformations to generate a subject-to-subject transformation matrix:

```
# Affine transformations allowing us
# to move coordinates between spaces
s1func2struc = np.loadtxt('data/xfms/subj1/example_func2highres.mat')
s1struc2std  = np.loadtxt('data/xfms/subj1/highres2standard.mat')
s2func2struc = np.loadtxt('data/xfms/subj2/example_func2highres.mat')
s2struc2std  = np.loadtxt('data/xfms/subj2/highres2standard.mat')
```


Let's define a couple of functions to help us.  A Python function is defined
with `def function_name(arguments):`, and the entire function body must be
indented, just like with `if`, `else`, and `for` statements.


```
# The '*' before xforms allows us
# to pass any number of transformation
# matrices to this function

def concat(*xforms):
    """Combines the given matrices (returns the dot product)."""

    result = xforms[0]

    for i in range(1, len(xforms)):
        result = result @ xforms[i]

    return result


def transform(xform, coord):
    """Transform the given coordinates with the given affine. """
    return np.dot(xform[:3, :3], coord) + xform[:3, 3]
```

Now we can use the `concat` function to create a subject-to-subject
transformation matrix, and then the `transform` function to transform
our ROI coordinates:


```
import numpy.linalg as npla

s1func2std    = concat(s1struc2std, s1func2struc)
s2func2std    = concat(s2struc2std, s2func2struc)
s2std2func    = npla.inv(s2func2std)
s1func2s2func = concat(s2std2func, s1func2std)

print('Subject 1 functional -> Subject 2 functional affine:')
print(s1func2s2func)

for c in subj1rois:
    xc = transform(s1func2s2func, c)
    c  = '{:6.2f} {:6.2f} {:6.2f}'.format(*c)
    xc = '{:6.2f} {:6.2f} {:6.2f}'.format(*xc)
    print('Transform: [{}] -> [{}])'.format(c, xc))
```


---
<a class="anchor" id="loading-mri-data"></a>
# Loading MRI data


MRI data consists of a structured 3D (space) or 4D (space+time) grid of data
points. In FSL, we store images in the NIfTI file format. We can use a library
called `nibabel` to load images into Python, and access their data as `numpy`
arrays:


```
import nibabel as nib

image = nib.load('data/fa_map.nii.gz')
data = image.get_data()

print('Image shape:          ', image.shape)
print('Voxel dimensions: (mm)', image.header.get_zooms())
print('Data range:           ', data.min(), data.max())
```


It is also easy to save image data as a `.nii.gz` file. Make sure to specify
an appropriate image header when you create a new image - this ensures that
the image orientation is saved correctly:

```
fmri = nib.load('data/fmri/01.nii.gz')
data = fmri.get_data()
mean = data.mean(axis=3)
mean = nib.Nifti1Image(mean, None, fmri.header)

nib.save(mean, '01_mean_func.nii.gz')
```

Let's take a quick look at the image we just saved in FSLeyes (remember the
`!` trick we can use in Jupyter notebooks):


```
!fsleyes data/fmri/01 01_mean_func -cm red-yellow
```


## Example - mean ROI time series for a group of subjects


Imagine that we have some fMRI data for a group of individuals, and we are
interested in hippocampal activity. Here's how we can extract the mean time
series in the hippocampal region for each of our subjects.

> We are using a couple of standard libraries here - `glob` allows us to
> search for file names with wildcard patterns, and `os.path` contains a
> variety of functions for working with file paths.


```
import os.path as op
from glob import glob

ts_files = glob('data/fmri/??.nii.gz')

timeseries = {}

for ts_file in ts_files:
    print('Extracting timeseries from', ts_file)
    subj_id = op.basename(ts_file)[:2]
    mask_file = 'data/fmri/{}_hipp_mask.nii.gz'.format(subj_id)
    data = nib.load(ts_file).get_data()
    mask = nib.load(mask_file).get_data()
    timeseries[subj_id] = data[mask > 0].mean(axis=0)
```


We now have a dict which contains the mean hippocampal BOLD activity for each
of our subjects:

```
for subj, ts in timeseries.items():
    print('Subject {}: {} samples, mean: {:0.2f}, std. dev.: {:0.2f}'.format(
        subj, len(ts), ts.mean(), ts.std()))
```


---
<a class="anchor" id="plotting-with-matplotlib"></a>
# Plotting with matplotlib


The main plotting module in python is `matplotlib`.  There is a lot that can
be done with it - see the [gallery](https://matplotlib.org/gallery/index.html).


To use `matplotlib` inside a jupyter notebook, we need to run a special
command:

```
%matplotlib inline
```

This is only necessary inside notebooks - it doesn't need to be done if you
are writing a normal Python script.


`matplotlib` works very similarly to plotting in matlab.  Let's start with
some simple examples.


<a class="anchor" id="2d-plots"></a>
## 2D plots


```
import matplotlib.pyplot as plt

# plt.style.use('bmh')

x = np.linspace(-np.pi, np.pi, 256)
cosx, sinx = np.cos(x), np.sin(x)

plt.plot(x, cosx)
plt.plot(x, sinx, color='red', linewidth=4, linestyle='-.')
plt.plot(x, sinx**2)
plt.xlim(-np.pi, np.pi)
plt.title('Our first plots')
```


> Note that the `plt.style.use('bmh')` command is not necessary, but it
> does make nicer looking plots in general.  You can use `ggplot`
> instead of `bmh` if you want something resembling plots made by R.
> For a list of options run: `print(plt.style.available)`


Let's plot the hippocampal time series we extracted in the previous section:

```
tr = nib.load('data/fmri/01.nii.gz').header.get_zooms()[3]
for subj, ts in timeseries.items():
    secs = np.arange(len(ts)) * tr
    plt.plot(secs, ts, lw=0.5, label='subject {}'.format(subj))

plt.xlabel('Time (seconds)')
plt.ylabel('BOLD')
plt.title('Mean hippocampal BOLD activity')
plt.legend()
```


You can also save the objects and interrogate/set their properties, as
well as those for the general axes:

```
hdl = plt.plot(x, cosx)
print(hdl[0].get_color())
hdl[0].set_color('#707010')
hdl[0].set_linewidth(0.5)
plt.grid(False)
```


Use `dir()` or `help()` or the online docs to get more info on what
you can do with these.


<a class="anchor" id="histograms-and-bar-charts"></a>
## Histograms and bar charts


For a simple histogram you can do this:

```
r = np.random.rand(1000)
n,bins,_ = plt.hist((r-0.5)**2, bins=30)
```


where it also returns the number of elements in each bin, as `n`, and
the bin centres, as `bins`.


> The `_` in the third part on the left
> hand side is a shorthand for just throwing away the corresponding part
> of the return structure.


There is also a call for doing bar plots:

```
samp1 = r[0:10]
samp2 = r[10:20]
bwidth = 0.3
xcoord = np.arange(10)
plt.bar(xcoord-bwidth, samp1, width=bwidth, color='red', label='Sample 1')
plt.bar(xcoord, samp2, width=bwidth, color='blue', label='Sample 2')
plt.legend(loc='upper left')
```


<a class="anchor" id="scatter-plots"></a>
## Scatter plots


It would be possible to use `plot()` to create a scatter plot, but
there is also an alternative: `scatter()`

```
fig, ax = plt.subplots()

# scale the size of each point according
# to the difference between the two samples
ssize = np.abs(samp1-samp2)
ssize = 50 + 250 * (ssize - ssize.min()) / (ssize.max() - ssize.min())

ax.scatter(samp1, samp2, s=ssize, alpha=0.5)

# we'll also add a y==x line for reference
allsamps = np.hstack((samp1,samp2))
ax.plot([min(allsamps),max(allsamps)],[min(allsamps),max(allsamps)], color='red', linestyle='--')
plt.xlim(min(allsamps),max(allsamps))
plt.ylim(min(allsamps),max(allsamps))
```


> Note that in this case we use the first line return to get a handle to
> the axis, `ax`, and the figure ,`fig`. Most things in `matplotlib` can
> be done through the `plt` interface, but working directly with the `fig`
> and `ax` objects gives more flexibility.


<a class="anchor" id="subplots"></a>
## Subplots


These are very similar to matlab:


```
plt.subplot(2, 1, 1)
plt.plot(x,cosx, '.-')
plt.xlim(-np.pi, np.pi)
plt.ylabel('Full sampling')
plt.subplot(2, 1, 2)
plt.plot(x[::30], cosx[::30], '.')
plt.xlim(-np.pi, np.pi)
plt.ylabel('Subsampled')
```


<a class="anchor" id="displaying-images"></a>
## Displaying images


The main command for displaying images is `imshow()`


```
nim = nib.load(op.expandvars('${FSLDIR}/data/standard/MNI152_T1_1mm.nii.gz'), mmap=False)
imdat = nim.get_data().astype(float)
plt.imshow(imdat[:,:,70], cmap=plt.cm.gray)
plt.colorbar()
plt.grid(False)
```


<a class="anchor" id="example-image-summary-plots"></a>
## Example - image summary plots


Let's combine some of the techniques we have learnt, and plot some slices from
the mean functional image, and overlay the hippocampal mask, for each of our
subjects:


```
ts_files = glob('data/fmri/??.nii.gz')

timeseries = {}

for i, ts_file in enumerate(ts_files):
    subj_id = op.basename(ts_file)[:2]
    mask_file = 'data/fmri/{}_hipp_mask.nii.gz'.format(subj_id)

    func = nib.load(ts_file).get_data().mean(axis=3)
    mask = nib.load(mask_file).get_data()

    xlen, ylen, zlen = func.shape

    xslice = int(xlen / 3)
    yslice = int(ylen / 2)
    zslice = int(zlen / 3)

    # We use the np.flipud function
    # to flip each slice, and the T
    # (short for transpose) attribute
    # to rotate each slice, so they
    # are all oriented sensibly.
    fx = np.flipud(func[xslice, :, :].T)
    mx = np.flipud(mask[xslice, :, :].T)
    fy = np.flipud(func[:, yslice, :].T)
    my = np.flipud(mask[:, yslice, :].T)
    fz = np.flipud(func[:, :, zslice].T)
    mz = np.flipud(mask[:, :, zslice].T)

    plt.subplot(5, 3, (i * 3) + 1)
    plt.imshow(fx, cmap=plt.cm.gray)
    plt.imshow(mx, cmap=plt.cm.hot, alpha=0.6)

    plt.subplot(5, 3, (i * 3) + 2)
    plt.imshow(fy, cmap=plt.cm.gray)
    plt.imshow(my, cmap=plt.cm.hot, alpha=0.6)

    plt.subplot(5, 3, (i * 3) + 3)
    plt.imshow(fz, cmap=plt.cm.gray)
    plt.imshow(mz, cmap=plt.cm.hot, alpha=0.6)
plt.gcf().set_size_inches(6, 12)
```


<a class="anchor" id="3d-plots"></a>
## 3D plots


```
# Taken from https://matplotlib.org/gallery/mplot3d/wire3d.html#sphx-glr-gallery-mplot3d-wire3d-py

from mpl_toolkits.mplot3d import axes3d

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')

# Grab some test data.
X, Y, Z = axes3d.get_test_data(0.05)

# Plot a basic wireframe.
ax.plot_wireframe(X, Y, Z, rstride=10, cstride=10)
```


Surface renderings are many other plots are possible - see 3D examples on the
[matplotlib
webpage](https://matplotlib.org/gallery/index.html#mplot3d-examples-index)


---
<a class="anchor" id="writing-and-running-scripts"></a>
# Writing and running scripts


So far we have been running Python code within a Jupyter notebook. This is all
well and good for educational sessions, or for learning/experimenting, but at
some stage we're going to want to write a Python program that we can run from
the command-line.


A Python program (or script) is a plain-text file, just like a Bash or MATLAB
script. The first line of a python script is usually:


```
#!/usr/bin/env python
```


which invokes whichever version of python can be found by `/usr/bin/env` since
python can be located in many different places.

In FSL, we have our own version of Python, which has all of the libraries that
we need already installed.  To use this version we can use the line:


```
#!/usr/bin/env fslpython
```


After this line the rest of the file just uses regular Python code. Make sure
you make the file executable - run `chmod a+x` on it, just like you would with
a bash script.


<a class="anchor" id="command-line-arguments"></a>
## Command line arguments

The simplest way of dealing with command line arguments is use the module
`sys`, which gives access to an `argv` list.


> Note that the first element in `sys.argv` is the name of the script.


> You can run this code block in a terminal by running `data/print_argv`.


```
#!/usr/bin/env python
import sys
print('Number of arguments:', len(sys.argv))
print('Arguments:')
print('\n'.join(sys.argv))
```


For more sophisticated argument parsing you can use `argparse` - good
documentation and examples of this can be found [on the
web](https://docs.python.org/3/library/argparse.html).


> `argparse` can automatically produce help text for the user, validate input
> etc., so it is strongly recommended for more complex scripts.


<a class="anchor" id="example-fslstats"></a>
## Example - `fslstats`


Let's do something useful - we'll put together several of the concepts covered
so far, and write a replacement for `fslstats`.


> `fslstats` is a FSL utility which calculates summary statistics on an image,
> e.g. mean, min, max, etc.

> You can find this script at `data/pyfslstats`.


```
#!/usr/bin/env fslpython

import nibabel as nib
import numpy as np
import sys

def main():
    if len(sys.argv) < 2:
        print()
        print('usage: pyfslstats image [options]')
        print('Available options:')
        print('  -R           : output <min intensity> <max intensity>')
        print('  -v           : output <voxels> <volume>')
        print('  -V           : output <voxels> <volume> (for nonzero voxels)')
        print('  -m           : output mean')
        print('  -M           : output mean (for nonzero voxels)')
        print()
        sys.exit(0)

    imgfile = sys.argv[1]
    img = nib.load(imgfile)

    for opt in sys.argv[2:]:
        if opt == '-R':
            minmax(img)
        elif opt == '-v':
            volume(img, True)
        elif opt == '-V':
            volume(img, False)
        elif opt == '-m':
            mean(img, True)
        elif opt == '-M':
            mean(img, False)

def minmax(img):
    data = img.get_data()
    print(data.min(), data.max())

def volume(img, includeZeros):

    data = img.get_data()

    if not includeZeros:
        data = data[data != 0]

    # size in mm of one voxel
    xl, yl, zl = img.header.get_zooms()[:3]
    voxdim = xl * yl * zl

    nvoxels = np.prod(data.shape)
    volume  = nvoxels * voxdim

    print(nvoxels, volume)

def mean(img, includeZeros):

    data = img.get_data()

    if not includeZeros:
        data = data[data != 0]

    print(data.mean())

if __name__ == '__main__':
    main()
```