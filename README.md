# Introduction to data analysis in Python

This repository contains short introductions to Python data analysis and machine learning for the 2019 FMRIB Graduate Programme.

## Content

    - intro_talk.ipynb
    - pandas.ipynb
    - scikit_learn.ipynb
    - keras1.ipynb
    - keras2.ipynb

## Preparation

The talks are in the form of an interactive [Jupyter
notebook](https://www.jupyter.org).  If you would like to follow along on your
own laptops, please follow the below instructions.

### Updating from a previous tutorial

       git stash  # this removes any changes you made to the existing notebooks
       git pull

### New installations 

These instructions are for macOS or Linux (or Windows, if you are using a VM
or the Windows Subsystem for Linux), and assume that you have FSL 5.0.11 or
newer installed:


1. Choose a location on your file system. E.g.:

       mkdir -p ~/grad_course/python_intro
       cd ~/grad_course/python_intro


2. Download the *conda environment file* - this contains a list of Python
   libraries to install. 
   
   On Mac:

       curl https://git.fmrib.ox.ac.uk/paulmc/grad_course_2019_python_intro/raw/master/env.yml > env.yml

   On Linux:

       wget https://git.fmrib.ox.ac.uk/paulmc/grad_course_2019_python_intro/raw/master/env.yml


3. Create the Python environment. This will download about 1GB of data, and will ~5 min with a good
   internet connection:

       $FSLDIR/fslpython/bin/conda env create -f env.yml -p ./python_env


4. Download a small data set for use in the first talk (*you can skip this step for the other pracs*). 

    On Mac:

       curl -L https://users.fmrib.ox.ac.uk/~paulmc/data.tar.gz > data.tar.gz

    On Linux:

       wget http://users.fmrib.ox.ac.uk/~paulmc/data.tar.gz


5. Clone a copy of this repository

       git clone https://git.fmrib.ox.ac.uk/paulmc/grad_course_2019_python_intro.git
       cd grad_course_2019_python_intro

   > If you don't have `git`, find the *Download* button on the [gitlab
   > page](http://git.fmrib.ox.ac.uk/paulmc/grad_course_2019_python_intro) to
   > download a copy of the repository.


6. Install the data you downloaded in step 4 (*you can skip this step for the other pracs*):

       tar xf ../data.tar.gz


## Running the notebook


Open a terminal, navigate to the git repository you
cloned earlier, and run these commands:


    source $FSLDIR/fslpython/bin/activate ../python_env
    jupyter notebook

